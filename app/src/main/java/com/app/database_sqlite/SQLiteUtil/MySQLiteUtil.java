package com.app.database_sqlite.SQLiteUtil;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

public class MySQLiteUtil extends SQLiteOpenHelper {
//    private String DB_NAME = "mySQLite.db"; // 数据库名称
    private String TABLE_NAME = "student";  // 表名

    // SQLite语句
    private final String CREATE_TABLE_SQL = "create table " + TABLE_NAME + " (id integer primary key autoincrement, name text, number text, sex text, score text);";

    /** MySQLite的构造方法
     * @param context 上下文
     * @param dbName 数据库名称
     * @param dbFactory 数据库工厂
     * @param version 数据库版本
     */
    public MySQLiteUtil(Context context, String dbName, SQLiteDatabase.CursorFactory dbFactory, int version, String tableName) {
        super(context, dbName, dbFactory, version);
//        this.DB_NAME = dbName;
        this.TABLE_NAME = tableName;
    }

    /**创建时会被调用一次这个方法*/
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // 创建表
        sqLiteDatabase.execSQL(CREATE_TABLE_SQL);
    }

    /**数据库升级时会调用一次这个方法*/
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /** 增添数据
     * @param student 指定增加数据所使用的对象
     * @return 返回被写入的id
     */
    public long insertDate(Student student){
        // 获取一个可写的数据库
        SQLiteDatabase db = getWritableDatabase();

        // 创建键值对，并打包数据
        ContentValues values = new ContentValues();
        values.put("name",student.getName());       // 向键值对写入名字
        values.put("number",student.getNumber());   // 向键值对写入学号
        values.put("sex",student.getSex());         // 向键值对写入性别
        values.put("score",student.getScore());     // 向键值对写入成绩

        // 向指定表写入打包的数据
        return db.insert(TABLE_NAME,null,values);
    }

    /**删除数据
     * @param number 按学号删除学生信息
     * @return 返回删除的条目
     */
    public int deleteDate(String number){
        // 获取一个可写的数据库
        SQLiteDatabase db = getWritableDatabase();

        // 向指定表删除指定数据的条目
        return db.delete(TABLE_NAME,"number like ? ",new String[]{number});
    }

    /**修改数据
     * @param student 学生对象
     * @return 返回影响的条数
     */
    public int updateDate(Student student){
        // 获取一个可写的数据库
        SQLiteDatabase db = getWritableDatabase();

        // 创建键值对，并打包数据
        ContentValues values = new ContentValues();
        values.put("name",student.getName());       // 向键值对写入名字
        values.put("number",student.getNumber());   // 向键值对写入学号
        values.put("sex",student.getSex());         // 向键值对写入性别
        values.put("score",student.getScore());     // 向键值对写入成绩

        // 向指定表更新打包的内容
        return db.update(TABLE_NAME,values,"number like ?",new String[]{student.getNumber()});
    }

    /**根据number来查找数据
     * @param number 需要被查找的数据
     * @return 返回这个数据对应的List列表
     */
    @SuppressLint("Range")
    public List<Student> queryDate(String number){
        // 创建一个Student为数据的List
        List<Student> studentList = new LinkedList<>();

        // 获取一个可写的数据库
        SQLiteDatabase db = getWritableDatabase();

        // 向指定表查找指定数据的条目，返回的是一个指针
        Cursor cursor = db.query(TABLE_NAME,null,"number like ?",new String[]{number},null,null,null);

        // 判断有没有数据
        if(cursor != null){
            // 判断下面有没有数据
            while(cursor.moveToNext()){
                // 获取数据
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String sex = cursor.getString(cursor.getColumnIndex("sex"));
                String score = cursor.getString(cursor.getColumnIndex("score"));

                // 将获取到的数据打包成对象，并添加进List中
                Student student = new Student(name,number,score,sex);
                studentList.add(student);
            }
        }

        // 将获取的数据返回出去
        return studentList;
    }

    /**查找数据库中的全部数据
     * @return 返回由Student组成的List列表
     */
    @SuppressLint("Range")
    public List<Student> queryAllDate(){
        // 创建一个Student为数据的List
        List<Student> studentList = new LinkedList<>();

        // 获取一个可写的数据库
        SQLiteDatabase db = getWritableDatabase();

        // 向指定表查找指定数据的条目，返回的是一个指针
        Cursor cursor = db.query(TABLE_NAME,null,null,null,null,null,null);

        // 判断有没有数据
        if(cursor != null){
            // 判断下面有没有数据
            while(cursor.moveToNext()){
                // 获取数据
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String number = cursor.getString(cursor.getColumnIndex("number"));
                String sex = cursor.getString(cursor.getColumnIndex("sex"));
                String score = cursor.getString(cursor.getColumnIndex("score"));

                // 将获取到的数据打包成对象，并添加进List中
                Student student = new Student(name,number,score,sex);
                studentList.add(student);
            }
        }

        // 将获取的数据返回出去
        return studentList;
    }
}
