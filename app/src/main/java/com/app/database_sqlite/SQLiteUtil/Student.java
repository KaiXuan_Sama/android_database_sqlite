package com.app.database_sqlite.SQLiteUtil;

/**
 * 项目名称：Database_SQLite
 * 创始人：Kai_Xuan
 * 创建时间：2022/9/27 16:33
 * 功能：
 */
public class Student {
    private String name;    // 性命
    private String number;  // 学号
    private String score;   // 成绩
    private String sex;     // 性别

    public Student(String name, String number, String score, String sex) {
        this.name = name;
        this.number = number;
        this.score = score;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", score='" + score + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
