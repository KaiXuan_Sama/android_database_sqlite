package com.app.database_sqlite.SQLiteActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.database_sqlite.MainActivity;
import com.app.database_sqlite.R;
import com.app.database_sqlite.SQLiteUtil.MySQLiteUtil;
import com.app.database_sqlite.SQLiteUtil.Student;

import java.util.List;

public class queryActivity extends AppCompatActivity {
    private EditText et_Number;
    private TextView tv_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);

        et_Number = findViewById(R.id.et_Number);
        tv_result = findViewById(R.id.tv_result);
    }

    public void query(View view) {
        // 获取学号
        String number = et_Number.getText().toString().trim();

        // 学号判空
        if(number.equals("")){
            Toast.makeText(this,"请输入要查找学生的学号",Toast.LENGTH_SHORT).show();
            return;
        }

        // 创建一个数据库对象，并指定上下文、数据库名称、数据库工厂、数据库版本、数据库表名
        MySQLiteUtil mySQLiteUtil = new MySQLiteUtil(
                this,
                MainActivity.DB_NAME,
                MainActivity.DB_FACTORY,
                MainActivity.DB_VERSION,
                MainActivity.TABLE_NAME);

        // 通过学号查询数据
        List<Student> studentList = mySQLiteUtil.queryDate(number);

        // 数据判空
        if(studentList.size() == 0){
            Toast.makeText(this,"没有找到这个学号的学生",Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuilder Show = new StringBuilder("查询结果：\n");
        for (Student student : studentList){
            Show.append("   姓名：").append(student.getName())
                    .append(" 学号：").append(student.getNumber())
                    .append(" 性别：").append(student.getSex())
                    .append(" 成绩：").append(student.getScore()).append("\n");
        }

        // 更新显示数据
        tv_result.setText(Show);
    }

    public void queryAll(View view) {
        // 创建一个数据库对象，并指定上下文、数据库名称、数据库工厂、数据库版本、数据库表名
        MySQLiteUtil mySQLiteUtil = new MySQLiteUtil(
                this,
                MainActivity.DB_NAME,
                MainActivity.DB_FACTORY,
                MainActivity.DB_VERSION,
                MainActivity.TABLE_NAME);

        // 通过学号查询数据
        List<Student> studentList = mySQLiteUtil.queryAllDate();

        // 数据判空
        if(studentList.size() == 0){
            Toast.makeText(this,"数据库中没有学生",Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuilder Show = new StringBuilder("查询结果：\n");
        for (Student student : studentList){
            Show.append("   姓名：").append(student.getName())
                    .append(" 学号：").append(student.getNumber())
                    .append(" 性别：").append(student.getSex())
                    .append(" 成绩：").append(student.getScore()).append("\n");
        }

        // 更新显示数据
        tv_result.setText(Show);
    }
}