package com.app.database_sqlite.SQLiteActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.app.database_sqlite.MainActivity;
import com.app.database_sqlite.R;
import com.app.database_sqlite.SQLiteUtil.MySQLiteUtil;
import com.app.database_sqlite.SQLiteUtil.Student;

public class updateActivity extends AppCompatActivity {
    private EditText et_name,et_number,et_score;
    private RadioButton rb_man,rb_woman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        initView(); // 初始化布局
    }

    // view初始化
    private void initView() {
        // 控件绑定
        et_name = findViewById(R.id.et_name);
        et_number = findViewById(R.id.et_Number);
        et_score = findViewById(R.id.et_score);
        rb_man = findViewById(R.id.rb_man);
        rb_woman = findViewById(R.id.rb_woman);
    }

    public void update(View view) {
        // 获取输入框内的内容
        String name = et_name.getText().toString().trim();
        String number = et_number.getText().toString().trim();
        String score = et_score.getText().toString().trim();
        String sex = null;
        if(rb_man.isChecked()){
            sex = "男";
        }else if(rb_woman.isChecked()){
            sex = "女";
        }

        // 数据判空
        if(number.equals("")){    // 学号判空
            Toast.makeText(this,"请填写学号",Toast.LENGTH_SHORT).show();
            return;
        }else if(name.equals("")){            // 姓名判空
            Toast.makeText(this,"请填写姓名",Toast.LENGTH_SHORT).show();
            return;
        }else if(sex == null){          // 性别判空
            Toast.makeText(this,"请勾选性别",Toast.LENGTH_SHORT).show();
            return;
        }else if(score.equals("")){     // 成绩判空
            Toast.makeText(this,"请填写成绩",Toast.LENGTH_SHORT).show();
            return;
        }

        // 创建一个Student对象，并将上述获取到的值传递赋值给Student对象
        Student student = new Student(name,number,score,sex);

        // 创建一个数据库对象，并指定上下文、数据库名称、数据库工厂、数据库版本、数据库表名
        MySQLiteUtil mySQLiteUtil = new MySQLiteUtil(
                this,
                MainActivity.DB_NAME,
                MainActivity.DB_FACTORY,
                MainActivity.DB_VERSION,
                MainActivity.TABLE_NAME);

        // 更新打包好的数据
        int rowid = mySQLiteUtil.updateDate(student);
        if(rowid > 0){
            Toast.makeText(this,"成功修改了" + rowid + "个学生的信息",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"修改失败，可能并没有这个学生",Toast.LENGTH_SHORT).show();
        }
    }
}