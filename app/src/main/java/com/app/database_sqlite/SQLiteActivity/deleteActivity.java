package com.app.database_sqlite.SQLiteActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.database_sqlite.MainActivity;
import com.app.database_sqlite.R;
import com.app.database_sqlite.SQLiteUtil.MySQLiteUtil;

public class deleteActivity extends AppCompatActivity {
    private EditText et_Number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);

        et_Number = findViewById(R.id.et_Number);
    }

    /**删除按钮*/
    public void delete(View view) {
        // 获取学号
        String number = et_Number.getText().toString().trim();

        // 学号判空
        if(number.equals("")){
            Toast.makeText(this,"请输入要删除学生的学号",Toast.LENGTH_SHORT).show();
            return;
        }

        // 创建一个数据库对象，并指定上下文、数据库名称、数据库工厂、数据库版本、数据库表名
        MySQLiteUtil mySQLiteUtil = new MySQLiteUtil(
                this,
                MainActivity.DB_NAME,
                MainActivity.DB_FACTORY,
                MainActivity.DB_VERSION,
                MainActivity.TABLE_NAME);

        // 通过学号删除数据
        int rowid = mySQLiteUtil.deleteDate(number);
        if(rowid > 0){
            Toast.makeText(this,"成功删除" + rowid + "个学生",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"删除失败，可能没找到这个学生",Toast.LENGTH_SHORT).show();
        }
    }
}