package com.app.database_sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import com.app.database_sqlite.SQLiteActivity.deleteActivity;
import com.app.database_sqlite.SQLiteActivity.insertActivity;
import com.app.database_sqlite.SQLiteActivity.queryActivity;
import com.app.database_sqlite.SQLiteActivity.updateActivity;

public class MainActivity extends AppCompatActivity {
    /**本地数据库名称*/
    public static final String DB_NAME = "mySQLite.db";
    /**本地数据库工厂*/
    public static final SQLiteDatabase.CursorFactory DB_FACTORY = null;
    /**本地数据库版本*/
    public static final int DB_VERSION = 1;
    /**本地数据库表名*/
    public static final String TABLE_NAME = "student";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void insertDate(View view) {
        Intent intent = new Intent(this, insertActivity.class);
        startActivity(intent);
    }

    public void deleteDate(View view) {
        Intent intent = new Intent(this, deleteActivity.class);
        startActivity(intent);
    }

    public void updateDate(View view) {
        Intent intent = new Intent(this, updateActivity.class);
        startActivity(intent);
    }

    public void queryData(View view) {
        Intent intent = new Intent(this, queryActivity.class);
        startActivity(intent);
    }
}